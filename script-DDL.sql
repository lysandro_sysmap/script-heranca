CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

drop table public."Inheritance"

create table public."Inheritance" (
	id uuid primary key DEFAULT uuid_generate_v4(),
	assignee_code_cn int4,
	assignee_name_cn varchar(255),
	assignor_code_cn int4 ,
	assignor_name_cn varchar(255),
	role_id int4,
	role_description varchar(100),
	role_start timestamp,
	role_end timestamp ,
	operational_cycle_start int4,
	operational_cycle_end int4,
	operational_cycles_count int4,
	occurrence_type_id int4,
	created_at timestamp,
	created_by varchar(50),
	last_updated_at timestamp,
	last_updated_by varchar(50)
);

-- delete from public."Inheritance"
select count(*) from public."Inheritance"


