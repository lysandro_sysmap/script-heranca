const { Client } = require("pg");
const moment = require("moment");
const csv = require("csv-parser");
const fs = require("fs");

const schema = "public";
const tableName = "Inheritance";

const readFile = async () => {
  records = [];
  const readStream = fs.createReadStream(`./planilha.csv`).pipe(
    csv({
      separator: ";",
    })
  );

  for await (const chunk of readStream) {
    records.push(chunk);
  }
  return records;
};

const transformRecordToNewTable = (records) =>
  records.map((record) => ({
    assignee_code_cn: Object.values(record)[0],  //workaround for record["CD_CONSULTORA"] value
    assignee_name_cn: record["NOME_CONSULTORA"],
    assignor_code_cn: record["CD_CONSULTORA_CEDENTE"],
    assignor_name_cn: record["NOME_CONSULTORA_CEDENTE"],
    role_id: record["CD_TIPO_PAPEL"],
    role_description: record["DESCRICAO_PAPEL"],
    role_start:
      record["DT_INICIO_PAPEL"] &&
      moment(record["DT_INICIO_PAPEL"], "DD/MM/YY").format("L"),
    role_end:
      record["DT_TERMINO_PAPEL"] &&
      moment(record["DT_TERMINO_PAPEL"], "DD/MM/YY").format("L"),
    operational_cycle_start: record["NM_CICLO_INICIO"],
    operational_cycle_end: record["NM_CICLO_FIM"],
    operational_cycles_count: record["QT_CICLO"],
    occurrence_type_id: record["ID_TIPO_OCORRENCIA"],
    created_by: record["CD_USUARIO"],
    created_at:
      record["DT_ULTIMA_ATUALIZACAO"] &&
      moment(record["DT_ULTIMA_ATUALIZACAO"], "DD/MM/YY").format("L"),
    last_updated_by: record["CD_USUARIO"],
    last_updated_at:
      record["DT_ULTIMA_ATUALIZACAO"] &&
      moment(record["DT_ULTIMA_ATUALIZACAO"], "DD/MM/YY").format("L"),
  }));

const treatColumnValue = (fieldsValues) => {
  return fieldsValues.map((value) => {
    if (!isNaN(value) && typeof value === "number") {
      return parseInt(value, 10);
    }
    if (value) {
      return `${value}`.trim();
    }

    return null;
  });
};

const insertItem = async (client, record) => {
  const recordKeys = Object.keys(record);
  const text = `INSERT INTO ${schema}."${tableName}"(${recordKeys.join(
    ", "
  )}) VALUES(${Array.from(
    { length: 16 },
    (_, index) => "$" + (index + 1)
  )}) RETURNING *`;

  const values = treatColumnValue(Object.values(record));

  try {
    const res = await client.query(text, values);
    console.log('Processing', res.rows[0].assignee_name_cn);
  } catch (err) {
    console.log({ error: err.stack, text, values });
  }
};

(async () => {
  const client = new Client({
    user: "postgres",
    host: "localhost",
    database: "postgres",
    password: "postgres",
    port: 5432,
  });

  client.connect();
  try {
    const records = await readFile();
    const newRecordsTransformed = transformRecordToNewTable(records);

    await client.query('BEGIN');

    for await (const record of newRecordsTransformed) {
      await insertItem(client, record);
    }

    const result = await client.query(`select count(*) as quantity from ${schema}."${tableName}"`)

    if (result.rows[0].quantity) {
      client.query('COMMIT');
    } else {
      throw new Error('executed ROLLBACK')
    }
  } catch (e) {
    client.query('ROLLBACK');
    throw e;
  } finally {
    await client.end();
    console.log('\n\nFinished');
    process.exit();
  }
})().catch(e => console.log(e));
